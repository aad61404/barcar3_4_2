import { _decorator, Component, resources, find, Button, director } from 'cc'
import { GameController } from './controllers/GameController'
import {Card} from './components/Cards'
import API from './api/Api'

const { ccclass, property } = _decorator

let warnTimer

@ccclass('GameApplication')
export class GameApplication extends Component {
    gameController: GameController
    cards: Cards
    Node_error


    start = () => {
        console.log('GameApplication start')

        this.logVersion()

        this.initApplication()

        // 假設開啟 preview 3秒後開始連線
        setTimeout(() => {
            this.#api_initialize()
        }, 3000)
    }

    logVersion = () => {
        try {
            resources.load('version', function (err, jsonAsset) {
                const { version } = jsonAsset.json
                console.log('====================')
                console.log('Client Version: ', version)
                console.log('====================')
                window.version = version
            })
        } catch (err) {
            console.warn('load project version failed')
        }
    }


    initApplication = () => {
        const loadingScreen = document.getElementById('loading')
        if (loadingScreen) {
            loadingScreen.style.display = 'none'
        }

        this.gameController = this.node.getComponentInChildren(GameController)
        this.gameController.initialize()

        // initialize cardsGroup
        this.cards = find('Node_Game/Node_CardsGroup', this.node)?.getComponentsInChildren(Card)
        this.cards.forEach((card, index) => card.init(index + 1))

        this.Node_error = find('Node_UI/Canvas/Node_error', this.node)
    }

    #api_initialize = async () => {
        API.initialize(this)
    }

    handleStatus_Idle = (content) => {
        const { table: { countDown } } = content
        this.dismissErrorMessage()
        this.cancelWarnTimer()
        this.onWarnTimer(countDown)
        this.gameController.handleStatus_Idle(content)
    }

    handleStatus_OpenForBet = (content) => {
        const { table: { countDown } } = content
        // Todo 30 秒開始下注
        // 發牌 (蓋著)
        this.gameController.handleStatus_OpenForBet(content)
        this.cancelWarnTimer()
        this.onWarnTimer(countDown)
    }

    handleStatus_Dealing = (content) => {
        const { table: { countDown } } = content
        // 開牌

        this.cancelWarnTimer()
        this.onWarnTimer(countDown)
        this.gameController.handleStatus_Dealing(content)
    }

    handleStatus_Result = (content) => {
        const { table: { countDown } } = content
        // 開牌

        this.cancelWarnTimer()
        this.onWarnTimer(countDown)
    }

    handleErrorMessage = () => {
        director.pause()
        this.Node_error!.active = true
    }

    dismissErrorMessage = () => {
        director.resume()
        this.Node_error!.active = false
    }


    onWarnTimer = (sec) => {
        const secTime = sec * 1000
        warnTimer = setTimeout(() => {
            this.handleErrorMessage()
        }, secTime)
    }

    cancelWarnTimer = () => {
        clearTimeout(warnTimer)
    }

}