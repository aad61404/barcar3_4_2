
import { Component } from 'cc';
import {
  LOGIN_REQUEST,
  LOGIN_RESPONSE,
  IDLE_REQUEST,
  IDLE_RESPONSE,
  OPEN_FOR_BET_REQUEST,
  OPEN_FOR_BET_RESPONSE,
  DEALING_REQUEST,
  DEALING_RESPONSE,
  RESULT_REQUEST,
  RESULT_RESPONSE,
  SHUFFING_REQUEST,
  SHUFFING_RESPONSE,
  SERVER_ERROR,
  ERROR_EVENT,
} from './cmdCode';

const webSocket_URL = "ws://localhost:3000";


//  Idle
const Idle_Response_Data = JSON.stringify({
  "cmdCode": 10021,
  "timeStamp": "2019-09-17T22:50:59.503Z",
  "seq": "b38e65ad-822e-44f0-a082-03847de7ece3",
  "content": {
    "errorCode": 0,
    "errorDescription": "Success"
  }
})

// Open For Bet
const Open_For_Bet_Response_Data = JSON.stringify({
  "cmdCode": 10031,
  "timeStamp": "2019-09-17T22:50:59.503Z",
  "seq": "b38e65ad-822e-44f0-a082-03847de7ece3",
  "content": {
    "errorCode": 0,
    "errorDescription": "Success"
  }
})

// Dealing
const Dealing_Response_Data = JSON.stringify({
  "cmdCode": 10041,
  "timeStamp": "2019-09-17T22:50:59.503Z",
  "seq": "b38e65ad-822e-44f0-a082-03847de7ece3",
  "content": {
    "errorCode": 0,
    "errorDescription": "Success"
  }
})

// Result
const Result_Response_Data = JSON.stringify({
  "cmdCode": 10051,
  "timeStamp": "2019-09-17T22:50:59.503Z",
  "seq": "b38e65ad-822e-44f0-a082-03847de7ece3",
  "content": {
    "errorCode": 0,
    "errorDescription": "Success"
  }
})

// Shuffing
const Shuffing_Response_Data = JSON.stringify({
  "cmdCode" : 10061,
  "timeStamp": "2019-09-17T22:50:59.503Z",
  "seq": "b38e65ad-822e-44f0-a082-03847de7ece3",
  "content": {
    "errorCode": 0,
    "errorDescription": "Success"
  }
})

const uuid = () => {
  var d = Date.now();
  if (
    typeof performance !== 'undefined' &&
    typeof performance.now === 'function'
  ) {
    d += performance.now(); //use high-precision timer if available
  }
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16);
  });
};

class Api {
  constructor() {
    this.mapper = {
      request: {
        [LOGIN_REQUEST]: LOGIN_RESPONSE,
      },
      response: {
        [IDLE_REQUEST]: this.#handlIdle,
        [OPEN_FOR_BET_REQUEST]: this.#handleOpenForBet,
        [DEALING_REQUEST]: this.#handleDealing,
        [RESULT_REQUEST]: this.#handleResult,
        [SHUFFING_REQUEST]: this.#handleShuffing,
        [SERVER_ERROR]: this.#forceCloseGame,
      },
    };
  }

  initialize = (gameApplication: Component) => {
    this.gameApplication = gameApplication;
    this.connect();
  };

  #login_request = () => {
    this.ws.send(JSON.stringify({
      "cmdCode": 10010,
      "timeStamp": "2019-09-17T22:50:59.503Z",
      "seq": "b38e65ad-822e-44f0-a082-03847de7ece3",
      "content": {
        "tableNo": this.tableNo,
        "force": this.force,
      }
    }))
  }

  connect = () => {
    const searchParams = new URLSearchParams(window.location.search);
    const queryServerUrl = searchParams?.get('serverUrl') || '';
    this.tableNo = searchParams?.get('tableNo') || '';
    this.force = searchParams?.get('force') === 'true';
    this.ws = new WebSocket(webSocket_URL);

    //開啟後執行的動作，指定一個 function 會在連結 WebSocket 後執行
    this.ws.onopen = () => {
      console.log("open connection");

      try {
        this.#login_request()
      } catch (error) {
        console.warn('error', error)
      }
      
    };

    this.ws.onmessage = (e) => {
      const receiveData = JSON.parse(e.data)
      const { cmdCode, content, seq } = receiveData
      
      console.log('receiveData:', receiveData)

      switch (cmdCode) {
        case 10011:
          console.log('receive 10011')
          break;

        case 10020:
          this.#handlIdle(content, seq)
          break;

        case 10030:
          this.#handleOpenForBet(content)
          break;

        case 10040:
          this.#handleDealing(content)
          break;

        case 10050:
          this.#handleResult(content)
          break;
        
        case 10060:
          this.#handleShuffing()
          break;

        case 19999:
          this.#forceCloseGame()
          break;
      }
    };

    //關閉後執行的動作，指定一個 function 會在連結中斷後執行
    this.ws.onclose = () => {
      console.log("close connection");
    };
  };

  // 10021
  #handlIdle = (content, seq?) => {
    let errorCode = 0;

    const hasError = errorCode !== 0;
    if (hasError) {
      this.gameApplication.setErrorMessage(errorCode)
    } else {
      this.gameApplication.handleStatus_Idle(content)
      this.ws.send(Idle_Response_Data)
    }
  }

  // 10031
  #handleOpenForBet = (content) => {
    this.gameApplication.handleStatus_OpenForBet(content)
    this.ws.send(Open_For_Bet_Response_Data)
  }

  // 10041
  #handleDealing = (content) => {
    this.gameApplication.handleStatus_Dealing(content)
    this.ws.send(Dealing_Response_Data)
  }

  // 10051
  #handleResult = (content) => {
    this.gameApplication.handleStatus_Result(content)
    this.ws.send(Result_Response_Data)
  }

  // 10061
  #handleShuffing = () => {
    this.ws.send(Shuffing_Response_Data)
  }

  // clsoe game
  #forceCloseGame = () => {
    console.warn('server force close game')
  }
}

export default new Api();