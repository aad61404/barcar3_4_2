// Baccarat OBS Streaming Protocol

export const LOGIN_REQUEST = 10010;
export const LOGIN_RESPONSE = 10011;

export const IDLE_REQUEST = 10020;
export const IDLE_RESPONSE = 10021;

export const OPEN_FOR_BET_REQUEST = 10030;
export const OPEN_FOR_BET_RESPONSE = 10031;

export const DEALING_REQUEST = 10040;
export const DEALING_RESPONSE = 10041;

export const RESULT_REQUEST = 10050;
export const RESULT_RESPONSE = 10051;

export const SHUFFING_REQUEST = 10060;
export const SHUFFING_RESPONSE = 10061;

export const SERVER_ERROR = 19999;

export const ERROR_EVENT = 99999;