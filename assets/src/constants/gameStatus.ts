// 發牌
export const DEAL_CARD = 'DEAL_CARD'
// 翻牌
export const FLIP_CARD = 'FLIP_CARD'
// 莊家補牌
export const BANKER_DRAW_CARD = 'BANKER_DRAW_CARD'
// 玩家補牌
export const PLAYER_DRAW_CARD = 'PLAYER_DRAW_CARD'
// 收牌
export const CLEAR_CARD = 'CLEAR_CARD'