
import { _decorator, Component, Animation, resources, Material, MeshRenderer, AudioSource } from 'cc';
const { ccclass, property } = _decorator;

const HEART = 'heart'
const DIAMOND = 'diamond'
const CLUB = 'club'
const SPADE = 'spade'

const SUIT_MAPPING = {
  S: SPADE,
  H: HEART,
  D: DIAMOND,
  C: CLUB,
}

const POINT_LIMIT = [
  'CA', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9', 'C1', 'CJ', 'CQ', 'CK',
  'DA', 'D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'D8', 'D9', 'D1', 'DJ', 'DQ', 'DK',
  'HA', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7', 'H8', 'H9', 'H1', 'HJ', 'HQ', 'HK',
  'SA', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8', 'S9', 'S1', 'SJ', 'SQ', 'SK',
];

const covertPokerMaterialName = (point:string) => {
  if(POINT_LIMIT.includes(point)) {
    const [suit, value] = point.split('')
    const suitMetrialName = SUIT_MAPPING[suit]
    return `${suitMetrialName}_${value}`
  }
}
@ccclass('Card')
export class Card extends Component {
  animation: Animation | null | undefined
  order: number = -1

  start() {
    this.animation = this.node.getComponent(Animation);
  }

  init(value: number) {
    this.order = value
  }

  // 發牌動畫
  playDealAnimation() {
    // TODO: 待確認動畫名
    const aniName = `place_card_${this.order}`
    this.animation?.play(aniName)
  }

  // 翻牌動畫
  playFlipAnimation() {
    // TODO: 待確認動畫名
    this.animation?.play(`flip_card`)
  }

  // 補牌動畫
  playDrawAnimation() {
    // TODO: 待確認動畫名，是否可由發牌+翻牌動畫組成
    this.animation?.play('draw_card')
  }

  // 替換花色點數
  changeCard(point:String) {
    // TODO: 待確認material檔名
    console.log('%c⧭', 'color: #00a3cc', 'changeCard');
    resources.load(`mtl_poker/mtl_${covertPokerMaterialName(point)}`, Material, (err, material) => {
      const meshRenderer = this.node.getComponentInChildren(MeshRenderer)
      if(meshRenderer) {
        meshRenderer.material = material;
      }
     });
  }

}