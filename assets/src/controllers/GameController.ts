import { _decorator, Component, Node } from 'cc';
import { CardsGroupController } from './CardsGroupController';
import { DollController } from './DollController';
const { ccclass, property } = _decorator;
import {CARDS_ORDER_LIST} from '../constants/cardsOrder'
import {DEAL_CARD, FLIP_CARD, BANKER_DRAW_CARD, PLAYER_DRAW_CARD, CLEAR_CARD} from '../constants/gameStatus'
@ccclass('GameController')
export class GameController extends Component {
    cardsGroupController: CardsGroupController;
    dollController: DollController;
    
    start = () => {
        console.log('GameController start');
        
        this.cardsGroupController = this.node.getComponentInChildren(CardsGroupController)
        this.dollController = this.node.getComponentInChildren(DollController)
        
        console.log('this.cardsGroupController:', this.cardsGroupController)
        console.log('this.dollController:', this.dollController)
        this.dollController.setCardsGroup(this.cardsGroupController)
        // TODO: 測試發牌動畫
        this.handleDollAnimationByStatus(DEAL_CARD)
    }

    initialize = async () => {
        console.log('GameController initialize');
    }
    
    sortCardsValueByOrder(valueList) {
        // valueList = ["C1","D3","D2","S4"]
        // CARDS_ORDER_LIST = [1,3,2,4]
        // output = ["C1", "D2", "D3", "S4"]
        return valueList.sort((prev, next)=> {
            const prevIndex = valueList.indexOf(prev) + 1
            const nextIndex = valueList.indexOf(next) + 1
            return CARDS_ORDER_LIST.indexOf(prevIndex) - CARDS_ORDER_LIST.indexOf(nextIndex)
        })
    }

    // trigger doll animation in different status
    handleDollAnimationByStatus(status:string) {
        this.dollController.playAnimationByStatus(status)
    }

    handleStatus_Idle = (content) => {
        // Todo clear
        // 人物 clear
        this.handleDollAnimationByStatus(CLEAR_CARD)
        // 牌 clear

    }

    handleStatus_OpenForBet = (content) => {
        // Todo 發牌
        // 人物 發牌
        this.handleDollAnimationByStatus(DEAL_CARD)
        // 牌 位移

    }

    handleStatus_Dealing = (content) => {
        // Todo 開牌 + 補牌
        const { game: { cards } } = content
        console.log('cards:', cards)
        const banker_cards = cards[0]
        const player_cards = cards[1]
        console.log('banker_cards:', banker_cards)
        console.log('player_cards:', player_cards)
        this.handleDollAnimationByStatus(FLIP_CARD)

        // doll => 事件帧 => Call 牌動畫

        if (banker_cards.length < 3 && player_cards.length < 3) {
            // 1. 都不補
            return 
        }else if (banker_cards.length > 2 && player_cards.length > 2) {
            // 2. 都補
        
        } else if (banker_cards.length < 3 && player_cards.length > 2) {
            // 3. 只補閒家
       
        } else if (banker_cards.length > 2 && player_cards.length < 3) {
            // 4. 只補莊家
        }
        
    }

    // card animation-event

}

