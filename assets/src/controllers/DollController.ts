import { CardsGroupController } from './CardsGroupController';
import { _decorator, Component, Animation } from 'cc';
const { ccclass, property } = _decorator;
import {DEAL_CARD, FLIP_CARD, BANKER_DRAW_CARD, PLAYER_DRAW_CARD, CLEAR_CARD} from '../constants/gameStatus'
@ccclass('DollController')
export class DollController extends Component {
    animation: Animation
    cardsGroupController: CardsGroupController

    onLoad() {
      this.animation = this.node.getComponent(Animation);
      this.initAnimaionFrameEvent()
    }

    start() {
    }

    update(deltaTime: number) {
        
    }

    initAnimaionFrameEvent() {
      // 試作使用幀事件控制card動畫
      this.animation.clips.forEach(clip => {
        if(clip?.name === 'deal_card') {
          clip?.events.push(
            {
              frame: 0.5,
              func: 'onTriggerDealCardsGroup',
              params: [1],
            },
            {
              frame: 1,
              func: 'onTriggerDealCardsGroup',
              params: [2],
            },
            {
              frame: 1.5,
              func: 'onTriggerDealCardsGroup',
              params: [3],
            },
            {
              frame: 2,
              func: 'onTriggerDealCardsGroup',
              params: [4],
            },
          )
          // 參考 https://forum.cocos.org/t/topic/136264/3
          clip.updateEventDatas()
          this.animation.createState(clip, clip.name);
        }
      })
    }

    setCardsGroup(controller: any) {
      this.cardsGroupController = controller
    }

    // 監聽設置的翻牌幀事件
    onTriggerDealCardsGroup(order: number) {
      console.log('%c⧭', 'color: #7f7700', 'onTriggerDealCardsGroup', order);
      this.cardsGroupController.handleTriggerDealing(order)
    }

    playAnimationByStatus(status:string) {
      switch(status) {
        case DEAL_CARD:
          this.playDealAnimation();
          break;
        case FLIP_CARD:
          this.playFilpAnimation();
          break;
        case BANKER_DRAW_CARD:
          this.playBankerDrawAnimation();
          break;
        case PLAYER_DRAW_CARD:
          this.playPlayerDrawAnimation();
          break;
        case CLEAR_CARD:
          this.playClearAnimation();
          break;
      }
    }

    // 待機動畫
    playIdleAnimation() {
      // TODO: 確認待機動作名稱
      this.animation?.play('idle')
    }

    // 發牌動畫
    playDealAnimation() {
      // TODO: 確認發牌動作名稱
      this.animation?.play('deal_card')
    }

    // 翻牌動畫
    playFilpAnimation() {
      // TODO: 確認翻牌動作名稱
      this.animation?.play('flip_card')
    }

    // 莊家補牌動畫
    playBankerDrawAnimation() {
      // TODO: 確認莊家補牌動作名稱
      this.animation?.play('banker_draw')
    }

    // 玩家補牌動畫
    playPlayerDrawAnimation() {
      // TODO: 確認玩家補牌動作名稱
      this.animation?.play('player_draw')
    }
    
    // 收牌動畫
    playClearAnimation() {
      // TODO: 確認收牌動作名稱
      this.animation?.play('clear_card')
    }

}

