import { _decorator, Component, find } from 'cc';
const { ccclass, property } = _decorator;
import {Card} from '../components/Cards'

@ccclass('CardsGroupController')
export class CardsGroupController extends Component {
    cards: Array | undefined

    start() {
        // TODO: 測試 card 接口
        this.cards = this.node.getComponentsInChildren(Card);
        this.cards.map(card => card.changeCard('CA'))
    }

    update(deltaTime: number) {
        
    }

    getCardByOrder(order: number) {
        const [card] = this.cards.filter(card => card.order === order)
        return card
    }

    // 發牌
    handleTriggerDealing(order: number) {
        this.getCardByOrder(order).playDealAnimation()
    }

    // 翻牌
    handleTriggerFliping(order: number) {
        this.getCardByOrder(order).playFlipAnimation()
    }

    // 玩家補牌
    handleTriggerPlayerDrawing() {
        this.getCardByOrder(5).playPlayerDrawAnimation()
    }

    // 莊家補牌
    handleTriggerBankerDrawing() {
        this.getCardByOrder(6).playBankerDrawAnimation()
    }
}

