//import express 和 ws 套件
const express = require("express");
const SocketServer = require("ws").Server;

//指定開啟的 port
const PORT = 3000;

//創建 express 的物件，並綁定及監聽 3000 port ，且設定開啟後在 console 中提示
const server = express().listen(PORT, () =>
  console.log(`Listening on ${PORT}`)
);

//將 express 交給 SocketServer 開啟 WebSocket 的服務
const wss = new SocketServer({ server });

const sleep = ms => {
  return new Promise((r, j) => {
    setTimeout(() => {
      r('reslove')
    }, ms)
  })
}

// varibles
let loginSuccess = 0;
let global_ws
let isShuffle = 0;
let nextGame = 1;


// ws send
const send = (data) => {
  global_ws.send(JSON.stringify(data));
}

// 10011
const loginResponse = (errorCode, counter) => {
  send(
    {
      "cmdCode": 10011,
      "timeStamp": new Date().toISOString(),
      "seq": "b38e65ad-822e-44f0-a082-03847de7ece3",
      "content": {
        "errorCode": errorCode,
        "errorDescription": "Success"
      }
    })
}

// 10020
const send_IdleData = () => {
  send({
    "cmdCode": 10020,
    "timeStamp": "2019-09-17T22:50:59.503Z",
    "seq": "b38e65ad-822e-44f0-a082-03847de7ece3",
    "content": {
      "table": {
        "id": 21,
        "countDown": 30,
        "status": "10"
      }
    }
  })
}

const send_OpenForBetData = () => {
  send({
    "cmdCode": 10030,
    "timeStamp": "2019-09-17T22:50:59.503Z",
    "seq": "b38e65ad-822e-44f0-a082-03847de7ece3",
    "content": {
      "table": {
        "id": 21,
        "countDown": 30,
        "status": "20"
      },
      "game": {
        "id": 312,
        "no": "PSM122031800D0",
        "endTime": "2019/09/14 16:23:10",
        "serverTime": "2019/09/14 16:23:10"
      }
    }
  })
}

// 10040
const send_DealingData = () => {
  send({
    "cmdCode": 10040,
    "timeStamp": "2019-09-17T22:50:59.503Z",
    "seq": "b38e65ad-822e-44f0-a082-03847de7ece3",
    "content": {
      "table": {
        "id": 21,
        "countDown": 30,
        "status": "40"
      },
      "game": {
        "id": 312,
        "no": "B11909140001",
        "endTime": "2019/09/14 16:23:10",
        "cards": [
          {
            "to": "Banker",
            "value": [
              "C1",
              "D8"
            ],
            "point": 9
          },
          {
            "to": "Player",
            "value": [
              "D2",
              "S5"
            ],
            "point": 7
          }
        ]
      }
    }
  })
}

// 10050
const send_ResultData = () => {
  send({
    "cmdCode": 10050,
    "timeStamp": "2019-09-17T22:50:59.503Z",
    "seq": "b38e65ad-822e-44f0-a082-03847de7ece3",
    "content": {
      "table": {
        "id": 21,
        "countDown": 30,
        "status": "40"
      },
      "game": {
        "id": 312,
        "no": "B11909140001",
        "endTime": "2019/09/14 16:23:10",
        "cards": [
          {
            "to": "Banker",
            "value": ["C1", "D8"],
            "point": 9
          },
          {
            "to": "Player",
            "value": ["D2", "S5"],
            "point": 7
          }
        ],
        "bigRoad": "B",
        "winTypes": ["Banker", "BankerPair"]
      }
    }
  })
}

// 10060
const send_ShuffingData = () => {
  send({
    "cmdCode": 10060,
    "timeStamp": "2019-09-17T22:50:59.503Z",
    "seq": "b38e65ad-822e-44f0-a082-03847de7ece3",
    "content": {
      "table": {
        "id": 21,
        "countDown": 30,
        "status": "60"
      }
    }
  })
}

//當 WebSocket 從外部連結時執行
wss.on("connection", (ws) => {
  //連結時執行此 console 提示
  console.log("Client connected");
  let counter = { i: -1 };
  global_ws = ws;

  ws.on("message", (e, isBinary = false) => {
    const receiveData = JSON.parse(e.toString())  // websocket 8.0 後 使用 toString 解析
    const { cmdCode } = receiveData
    console.log('receiveData:', receiveData)

    switch (cmdCode) {
      case 10010: {

        loginSuccess = 1;
        const errorCode = loginSuccess ? 0 : 1002;

        console.log('---- login success ----')
        // await sleep(1000)

        loginResponse(errorCode, counter);

        if (loginSuccess) {

          const startGame = async () => {
            
            await sleep(2000)
            await send_IdleData()

            await sleep(2000)
            await send_OpenForBetData()

            // 30秒下注
            await sleep(5000) // 測試使用之後移除
            // await sleep(30000)
            await send_DealingData()

            // Dealing 表演時間
            await sleep(0)
            // 5秒延遲
            await sleep(5000)
            await send_ResultData()
            
            // Result 表演時間
            await sleep(0)
            // 5秒延遲
            await sleep(5000)
            // 

            if (isShuffle) {
              // 5秒延遲
              await sleep(5000)
            }
            
            if (nextGame) {
              await sleep(5000)
            }

          }
          startGame()
        }
        break;
      }

      // Client發生問題時，通知Server
      case 99999: {
        send({
          "cmdCode": 99999,
          "timeStamp": "2019-09-17T22:50:59.503Z",
          "seq": "b38e65ad-822e-44f0-a082-03847de7ece3",
          "content": {
            "errorCode": 1000,
            "errorDescription": "Ball is stucking",
            "detail": ""
          }
        })
        break;
      }

    }
  });

  //當 WebSocket 的連線關閉時執行
  ws.on("close", () => {
    console.log("Close connected");
  });
});
