$DebugPreference = "Continue"


Write-Debug env:CI=$env:CI

if ([string]::IsNullOrEmpty($env:CI))
{
  Write-Debug 'TEST CI_COMMIT_BRANCH'
  $env:CI_COMMIT_BRANCH ='master'

  $env:BUILD_PATH = 'build/public/'

  $env:ROBOCOPY_DEST_PATH1 = '.'
  $env:ROBOCOPY_DEST_PATH2 = 'PATH2'

  $env:SSH_DEST_PATH1 = 'PATH1'
}


$BUILD_PATH=$env:BUILD_PATH
$SCP_BUILD_PATH=$BUILD_PATH + '*'


Function Run-Robocopy ($DEST_PATH) {
  if ( ![string]::IsNullOrEmpty($DEST_PATH) )
  {
    robocopy $BUILD_PATH $DEST_PATH /S /E /ETA /MT:50 /TEE /XX /LOG+:Log.txt
  }
}


Function Run-Scp ($DEST_PATH) {
  if ( ![string]::IsNullOrEmpty($DEST_PATH) )
  {
    scp -r $SCP_BUILD_PATH $DEST_PATH
  }
}


# for LOCAL DEBUG
Write-Debug CI_COMMIT_BRANCH_=$env:CI_COMMIT_BRANCH
Write-Debug BUILD_PATH=$BUILD_PATH
Write-Debug SCP_BUILD_PATH=$SCP_BUILD_PATH
Write-Debug ROBOCOPY_DEST_PATH1=$env:ROBOCOPY_DEST_PATH1
Write-Debug ROBOCOPY_DEST_PATH2=$env:ROBOCOPY_DEST_PATH2
Write-Debug ROBOCOPY_DEST_PATH3=$env:ROBOCOPY_DEST_PATH3
Write-Debug ROBOCOPY_DEST_PATH4=$env:ROBOCOPY_DEST_PATH4
Write-Debug SSH_DEST_PATH1=$env:SSH_DEST_PATH1
Write-Debug SSH_DEST_PATH2=$env:SSH_DEST_PATH2
Write-Debug SSH_DEST_PATH3=$env:SSH_DEST_PATH3
Write-Debug SSH_DEST_PATH4=$env:SSH_DEST_PATH4

# Robocopy
# Run-Robocopy($env:ROBOCOPY_DEST_PATH1)
# Run-Robocopy($env:ROBOCOPY_DEST_PATH2)
# Run-Robocopy($env:ROBOCOPY_DEST_PATH3)
# Run-Robocopy($env:ROBOCOPY_DEST_PATH4)